#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
    
    def test_eval_6(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_7(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)
    
    def test_eval_8(self):
        v = collatz_eval(3203, 5359)
        self.assertEqual(v, 238)

    def test_eval_9(self):
        v = collatz_eval(3791, 1200)
        self.assertEqual(v, 238)

    def test_eval_10(self):
        v = collatz_eval(6102, 5840)
        self.assertEqual(v, 187)

    def test_eval_11(self):
        v = collatz_eval(2032, 7544)
        self.assertEqual(v, 262)

    def test_eval_12(self):
        v = collatz_eval(7506, 5119)
        self.assertEqual(v, 262)

    def test_eval_13(self):
        v = collatz_eval(8022, 2838)
        self.assertEqual(v, 262)

    def test_eval_14(self):
        v = collatz_eval(9226, 6305)
        self.assertEqual(v, 257)

    def test_eval_15(self):
        v = collatz_eval(1390, 4327)
        self.assertEqual(v, 238)

    def test_eval_16(self):
        v = collatz_eval(2338, 7175)
        self.assertEqual(v, 262)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("39125 65203\n47056 76326\n96682 48978\n58277 61241\n54256 45086\n4642 66746\n8204 64054\n83211 15411\n87993 16645\n61721 39551\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "39125 65203 340\n47056 76326 340\n96682 48978 351\n58277 61241 335\n54256 45086 340\n4642 66746 340\n8204 64054 340\n83211 15411 351\n87993 16645 351\n61721 39551 340\n"
        )
    
    def test_solve_3(self):
        r = StringIO("55681 70425\n75981 46068\n40119 80937\n16011 30715\n59689 56682\n27171 86260\n46336 8794\n89280 30127\n571 96004\n23360 26999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "55681 70425 335\n75981 46068 340\n40119 80937 351\n16011 30715 308\n59689 56682 304\n27171 86260 351\n46336 8794 324\n89280 30127 351\n571 96004 351\n23360 26999 308\n"
        )
    
    def test_solve_4(self):
        r = StringIO("89339 99164\n31968 26849\n62828 58247\n77042 12274\n44566 86521\n69695 1971\n68373 15416\n73046 90945\n72462 78061\n92872 33017\n13020 99731\n68014 43966\n24576 45669\n28734 92030\n94217 68377\n12305 44381\n98035 31029\n63306 33413\n83169 63555\n57930 11944\n17919 7737\n30041 34345\n11848 2839\n20247 39726\n87616 91122\n45548 83275\n54666 81300\n71235 23538\n70029 25252\n60336 93904\n894 25837\n35998 1254\n41478 18676\n76599 15708\n78842 59321\n59323 98719\n80196 68091\n64539 84703\n55099 42628\n4871 9456\n9163 8933\n89411 59775\n91809 38825\n80082 8204\n75949 29795\n41366 62939\n18719 82166\n70999 59379\n62981 17076\n17850 11241\n97751 88932\n71805 86587\n5421 97722\n50815 38648\n68199 75446\n35105 68507\n24145 40546\n88951 74080\n35977 30255\n40758 9774\n5613 44287\n51723 44920\n56813 59502\n50933 29747\n98058 88161\n83193 21151\n69757 34064\n78016 7113\n4527 22449\n93396 7239\n93821 87052\n48082 20097\n87962 54820\n11765 99464\n64504 46834\n44355 26851\n9015 92493\n75397 30657\n76362 76366\n77345 613\n35081 56835\n9527 40531\n23376 47110\n24069 49103\n65266 67567\n83524 51755\n67941 32249\n5156 53603\n88332 87022\n52303 70002\n20886 40797\n47769 50851\n85755 40380\n79098 12146\n3437 70772\n67692 18043\n41951 41959\n82561 71107\n35314 33685\n16014 34388\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "89339 99164 333\n31968 26849 285\n62828 58247 335\n77042 12274 351\n44566 86521 351\n69695 1971 340\n68373 15416 340\n73046 90945 351\n72462 78061 351\n92872 33017 351\n13020 99731 351\n68014 43966 340\n24576 45669 324\n28734 92030 351\n94217 68377 351\n12305 44381 324\n98035 31029 351\n63306 33413 340\n83169 63555 351\n57930 11944 340\n17919 7737 279\n30041 34345 311\n11848 2839 268\n20247 39726 324\n87616 91122 333\n45548 83275 351\n54666 81300 351\n71235 23538 340\n70029 25252 340\n60336 93904 351\n894 25837 282\n35998 1254 324\n41478 18676 324\n76599 15708 340\n78842 59321 351\n59323 98719 351\n80196 68091 351\n64539 84703 351\n55099 42628 340\n4871 9456 262\n9163 8933 247\n89411 59775 351\n91809 38825 351\n80082 8204 351\n75949 29795 340\n41366 62939 340\n18719 82166 351\n70999 59379 335\n62981 17076 340\n17850 11241 279\n97751 88932 333\n71805 86587 351\n5421 97722 351\n50815 38648 314\n68199 75446 325\n35105 68507 340\n24145 40546 324\n88951 74080 351\n35977 30255 324\n40758 9774 324\n5613 44287 324\n51723 44920 314\n56813 59502 304\n50933 29747 324\n98058 88161 333\n83193 21151 351\n69757 34064 340\n78016 7113 351\n4527 22449 279\n93396 7239 351\n93821 87052 333\n48082 20097 324\n87962 54820 351\n11765 99464 351\n64504 46834 340\n44355 26851 324\n9015 92493 351\n75397 30657 340\n76362 76366 82\n77345 613 351\n35081 56835 340\n9527 40531 324\n23376 47110 324\n24069 49103 324\n65266 67567 268\n83524 51755 351\n67941 32249 340\n5156 53603 340\n88332 87022 333\n52303 70002 340\n20886 40797 324\n47769 50851 296\n85755 40380 351\n79098 12146 351\n3437 70772 340\n67692 18043 340\n41951 41959 182\n82561 71107 351\n35314 33685 311\n16014 34388 311\n"
        )






    
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
